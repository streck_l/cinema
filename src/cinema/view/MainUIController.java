package cinema.view;

//Imports
import cinema.ControlledScreen;
import cinema.Main;
import cinema.ScreensController;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This is the MainUIController Class. This Class handles all Actions in mainUI.fxml and sets the Button Graphics.
 * @author Lukas Streckeisen
 */
public class MainUIController implements Initializable, ControlledScreen{

    int movieID = 0;
    private ScreensController myController;

    /**
     * GridPane containing the MovieButtons
     */
    @FXML
    private GridPane grid;

    /**
     * Empty Constructor of MainUIController Class
     */
    public MainUIController(){

    }

    /**
     * Called by JavaFX.
     * Calls setButtonGraphics() to set Button Graphics
     */
    @Override
    public void initialize(URL url, ResourceBundle rb){
        setButtonGraphics();
    }

    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }

    private void setButtonGraphics(){
        ObservableList<Node> children = grid.getChildren();
        int imageCounter = 0;
        for (Node node: children){
            if (node instanceof Button){
                File f = new File(Main.movies.get(imageCounter).getImagepath());
                Image image = new Image(f.toURI().toString(), 140, 226, true, true);
                ImageView img = new ImageView(image);
                ((Button) node).setText("");
                ((Button) node).setGraphic(img);
                imageCounter++;
            }
        }
    }

    /**
     * Called by the MovieButtons when one of them is clicked
     * Opens TimeUI for selected Movie (now opens only an Alert)
     * @param event an ActionEvent given by JavaFX. event.getTarget() contains the Element, who triggered the event
     */
    public void handleMovieButton(ActionEvent event){
        // loops through all movie buttons in grid
        for(Node n: grid.getChildren()){
            // checks if child of grid is a button
            if (n instanceof Button){
                //checks if button is the clicked one
                if (event.getTarget() == n){
                    // this should open timeUI later
                    String content = "TimeUI should open instead of this later!\nMovie: " + Main.movies.get(movieID).getName();
                    Main.createAlert(Alert.AlertType.INFORMATION, "TimeGui", "MovieButton clicked", content);
                }
                movieID++;
            }
        }
    }

    /**
     * Called by the ExitButton when it's clicked. Closes the program.
     */
    public void handleExit(){
        System.exit(0);
    }

    public int getMovieID() {
        return movieID;
    }
}
