package cinema.view;

import cinema.ControlledScreen;
import cinema.ScreensController;
import cinema.model.Movie;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.net.URL;
import java.util.ResourceBundle;

public class TimeUIController implements Initializable, ControlledScreen {

    private ScreensController myController;
    private Movie movie;

    @FXML
    private Label labMovieName;
    @FXML
    private Label labWeekday;
    @FXML
    private Label labAgeRestriction;
    @FXML
    private Label labDuration;
    @FXML
    private ImageView imgMovie;
    @FXML
    private TextFlow txtfDescription;
    @FXML
    private Button btnAfternoon;
    @FXML
    private Button btnEevening;
    @FXML
    private Button btnBack;

    /**
     * Called by JavaFX
     * Sets Contents
     */
    @Override
    public void initialize(URL url, ResourceBundle rb){
        labMovieName.setText("");
        labAgeRestriction.setText("");
        labWeekday.setText("");
        labDuration.setText("");
        txtfDescription.getChildren().clear();
    }

    public void initUI(){
        labMovieName.setText(movie.getName());
        labWeekday.setText(movie.getDay());
        labAgeRestriction.setText(movie.getAgeRestriction());
        labDuration.setText(movie.getDuration());
        Text t = new Text(movie.getDescription());
        txtfDescription.getChildren().add(t);
        imgMovie = new ImageView("img\\deadpool.jpg");
    }

    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }

    public void setMovie(Movie movie){
        this.movie = movie;
    }

    public void handleShowSelected(ActionEvent event){
        if (event.getTarget() == btnAfternoon){
            System.out.println("do something");
        }
        else if (event.getTarget() == btnEevening){
            System.out.println("do something similar");
        }
    }

    public void handleBack(){
        System.out.println("Back");
    }

}
