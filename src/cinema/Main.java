package cinema;

//Imports
import cinema.json.Reader;
import cinema.model.Movie;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Main Class steers the program
 * @author Lukas Streckeisen
 */
public class Main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    public static final String MAIN_SCREEN = "mainUI";
    public static final String MAIN_SCREEN_FXML = "view/mainUI.fxml";
    public static final String TIME_SCREEN = "timeUI";
    public static final String TIME_SCREEN_FXML = "view/timeUI.fxml";

    public static ArrayList<Movie> movies;

    public Main(){
        movies = new ArrayList<>();
        Reader reader = new Reader();
        reader.readMovies();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        /*this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Cinema App");

        initRootLayout();

        showMainUI();*/

        ScreensController mainContainer = new ScreensController();
        mainContainer.loadScreen(MAIN_SCREEN, MAIN_SCREEN_FXML);
        //mainContainer.loadScreen(TIME_SCREEN, TIME_SCREEN_FXML);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Initializes the root layout
     */
    public void initRootLayout(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                .getResource("view/rootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            primaryStage.show();
        }
        catch (IOException ex){
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("The rootLayout could not loaded!\n\n" + Arrays.toString(ex.getStackTrace()));
            alert.showAndWait();
        }
    }

    /**
     * initializes the mainUI
     */
    public void showMainUI(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource("view/mainUI.fxml"));
            AnchorPane mainUi = (AnchorPane) loader.load();
            rootLayout.setCenter(mainUi);
        }
        catch (IOException ex){
            ex.printStackTrace();
            String content = "The mainGUI could not be loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            createAlert(Alert.AlertType.ERROR, "Error", "", content);
        }
    }

    /**
     * initializes the mainUI
     */
    public void showTimeUI(Movie movie){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource("view/timeUI.fxml"));
            AnchorPane timeUi = (AnchorPane) loader.load();
            rootLayout.setCenter(timeUi);
        }
        catch (IOException ex){
            ex.printStackTrace();
            String content = "The mainGUI could not be loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            createAlert(Alert.AlertType.ERROR, "Error", "",  content);
        }
    }

    public static void createAlert(Alert.AlertType type, String title, String headerText, String content){
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(content);
        alert.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
