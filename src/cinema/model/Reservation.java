/**
 * cinema reservation program
 * TBZ Module 226a
 * @author Lukas Streckeisen
 */

package cinema.model;

//Imports
import java.util.ArrayList;

public class Reservation {

    //declaration of variables

    private int movieID;
    private int showtime;
    private ArrayList<String> seats;
    private boolean isPaid;

    //constructor
    public Reservation(int movieID, int showtime, ArrayList<String> seats, boolean isPaid) {
        this.movieID = movieID;
        this.showtime = showtime;
        this.seats = seats;
        this.isPaid = isPaid;
    }


    //getters

    public int getMovieID() {
        return movieID;
    }

    public int getShowtime() {
        return showtime;
    }

    public ArrayList<String> getSeats() {
        return seats;
    }

    public boolean isPaid() {
        return isPaid;
    }
}
