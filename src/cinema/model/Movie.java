/**
 * cinema reservation program
 * TBZ Module 226a
 * @author Lukas Streckeisen
 */

package cinema.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Movie {

    //declaration of variables

    private String name;
    private String duration;
    private String ageRestriction;
    private String description;
    private int id;
    private String day;
    private String imagepath;

    //constructor
    public Movie(String day, String name, String duration, String ageRestriction, String description, int id, String imagepath){
        this.day = day;
        this.name = name;
        this.duration = duration;
        this.ageRestriction = ageRestriction;
        this.description = description;
        this.id = id;
        this.imagepath = imagepath;
    }


    //getters

    public String getName() {
        return name;
    }

    public String getDuration() {
        return duration;
    }

    public String getAgeRestriction() {
        return ageRestriction;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public String getDay() {
        return day;
    }

    public String getImagepath() {
        return imagepath;
    }
}
