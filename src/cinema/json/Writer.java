/**
 * cinema reservation program
 * TBZ Module 226a
 * @author Lukas Streckeisen
 */

package cinema.json;

//Imports
import cinema.model.Movie;
import cinema.model.Reservation;
import com.google.gson.Gson;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * The Writer Class handles all functions to write into files.
 * @author  Lukas Streckeisen
 */
public class Writer {

    //declaration of variables
    private String reservationPath = "resources/json/reservation.json";

    /**
     * Empty Constructor
     */
    public Writer(){
    }

    /**
     * adds new reservation(s) to reservationList and calls write function
     * @param movie Movie, who 'received' a new reservation
     * @param show string, containing the presentation time of the movie
     * @param reservations ArrayList<String> containing all reservation for the movie made now
     * @param isPaid boolean, true: purchase, cannot be canceled, false: reservation, can be canceled
     * @param reservationList ArrayList<Reservation>, containing all previous reservations for this movie
     */
    public void saveReservations(Movie movie, int show, ArrayList<String> reservations, boolean isPaid, ArrayList<Reservation> reservationList){
        Reservation res = new Reservation(movie.getId(), show, reservations, isPaid);
        reservationList.add(res);
        writeReservations(reservationList);
    }

    //saves all reservations and purchases

    /**
     * saves all reservations and purchases
     * @param reservationList ArrayList<Reservation>, containing all reservations for this movie
     * @return boolean, true: writing succeeded, false: writing failed
     */
    public boolean writeReservations(ArrayList<Reservation> reservationList){
        Gson gson = new Gson();
        try{
            FileWriter writer = new FileWriter(reservationPath);
            gson.toJson(reservationList, writer);
            writer.close();
            return true;
        }
        catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    //creates pdf document when seats purchased

    /**
     * Creates pdf bill, when a reservation has been purchased
     * @param filepath string, filepath to save the pdf
     * @param movie Movie, whose reservation has been purchased
     * @param seats ArrayList<String>, containing the purchased seats
     * @param time string, movie presentation time
     * @param price int, costs for the purchased seats
     * @return boolean, true: createPdf has succeeded, false: createPdf has failed
     */
    public boolean createPdf(String filepath, Movie movie, ArrayList<String> seats, String time, Double price) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(filepath));
            document.open();
            Font f1 = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 30);
            Font f2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            Paragraph paragraph = new Paragraph();
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setFont(f1);
            paragraph.add("Kino Luchs");
            document.add(paragraph);
            document.add(Chunk.NEWLINE);
            PdfPTable table = new PdfPTable(2);
            PdfPCell c1 = new PdfPCell(new Phrase("Filmname:"));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c1);
            String allSeats = "";

            for (String seat : seats){
                allSeats += seat+", ";
            }
            StringBuilder sb = new StringBuilder(allSeats);
            int index;
            index = allSeats.length();
            sb.deleteCharAt(index - 1);
            sb.deleteCharAt(index - 2);
            allSeats = sb.toString();

            c1 = new PdfPCell(new Phrase(movie.getName()));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c1);

            table.addCell("Filmdauer:");
            table.addCell(movie.getDuration());
            table.addCell("Filmbeginn");
            table.addCell(time);
            table.addCell("Altersfreigabe:");
            table.addCell(movie.getAgeRestriction());
            table.addCell("Filmbeschreibung:");
            table.addCell(movie.getDescription());
            table.addCell("Preis pro Sitz");
            table.addCell(Double.toString(price));
            table.addCell("Anzahl reservierte Sitze:");
            table.addCell(Integer.toString(seats.size()));
            table.addCell("Reservierte Plätze");
            table.addCell(allSeats);
            table.addCell("Gesamtpreis in Franken:");
            paragraph = new Paragraph();
            paragraph.setFont(f2);
            paragraph.add(Double.toString(seats.size()*price));
            table.addCell(paragraph);

            document.add(table);
            document.close();
            return true;

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    //setters

    public void setReservationPath(String reservationPath) {
        this.reservationPath = reservationPath;
    }
}
