package cinema.json;

//Imports
import cinema.Main;
import cinema.model.Movie;
import cinema.model.Reservation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * The Reader Class handles all functions to read from files.
 * @author Lukas Streckeisen
 */
public class Reader {

    //declaration of variables
    private String moviePath = "resources/json/movie.json";
    private String reservationPath = "resources/json/reservation.json";
    private boolean hadSucceeded;

    /**
     * Empty Constructor
     */
    public Reader(){
    }

    /**
     * reads all movie data from file
     * @return boolean, true: reading the file has succeeded, false: reading the file failed
     */
    public boolean readMovies() {
        Gson gson = new Gson();
        try {
            //reads and parses data from file and saves it to Main.movies ArrayList<Movie>
            Main.movies = gson.fromJson(new BufferedReader(new FileReader(moviePath)), new TypeToken<ArrayList<Movie>>() {}.getType());
            return true;
        }
        catch (Exception ex){
            ex.printStackTrace();
            Main.movies = null;
            return false;
        }
    }

    /**
     * reads all reservations from file
     * sets hadSucceeded to true, if reading has succeeded and to false if reading has failed
     * @return ArrayList<Reservation>, containing all reservations from file
     */
    public ArrayList<Reservation> getAllReservations() {
        ArrayList<Reservation> resOutput = new ArrayList<>();
        Gson gson = new Gson();
        try {
            resOutput = gson.fromJson(new BufferedReader(new FileReader(reservationPath)), new TypeToken<ArrayList<Reservation>>() {
            }.getType());
            hadSucceeded = true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            hadSucceeded = false;
        }
        if (resOutput == null){
            resOutput = new ArrayList<>();
        }
        return resOutput;
    }


    //Setters

    public void setMoviePath(String moviePath) {
        this.moviePath = moviePath;
    }

    public void setReservationPath(String reservationPath) {
        this.reservationPath = reservationPath;
    }


    //Getters
    //only for junit-tests
    public boolean hadSucceeded() {
        return hadSucceeded;
    }
}
