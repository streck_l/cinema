package cinema;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class ScreensController extends StackPane {

    private HashMap<String, Node> screens = new HashMap<>();

    public void addScreen(String name, Node screen){
        screens.put(name, screen);
    }

    public Node getScreen(String name){
        return screens.get(name);
    }

    public boolean loadScreen(String name, String resource){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));
            Parent loadScreen = (Parent) loader.load();
            ControlledScreen myScreenController =((ControlledScreen) loader.getController());
            myScreenController.setScreenParent(this);
            addScreen(name, loadScreen);
            return true;
        }
        catch (IOException ex){
            ex.printStackTrace();
            String content = "The " + name + " screen could not be loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            Main.createAlert(Alert.AlertType.ERROR, "Error", "", content);
            return false;
        }
    }

    public boolean setScreen(final String name){
        if (screens.get(name) != null){
            final DoubleProperty opacity = opacityProperty();

            if(!getChildren().isEmpty()){
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.INDEFINITE.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                getChildren().remove(0);
                                getChildren().add(0, screens.get(name));
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0))
                                        fadeIn.play();
                            }, new KeyValue(opacity, 0.0)));
        }
        else{
            Main.createAlert(Alert.AlertType.ERROR, "Error", "", "The " + name + " screen could'nt be loaded");
            return false;
        }
    }

    public boolean unloadScreen(String name){
        if (screens.remove(name) == null){
            System.err.println("Screen didn't exist");
            return false;
        }
        else {
            return true;
        }
    }

}
